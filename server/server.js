const express = require('express');
const fileUpload = require('express-fileupload');
const cors = require('cors')
const fs= require('fs')
// const path = require('path')

const app = express();
app.use(
  cors({
    origin: 'http://localhost:3000',
    credentials: true,
  })
);

app.use(fileUpload());

app.use(express.static('views'));
app.use(express.static('public'));

app.get('/',(req,res)=>{
  res.sendFile(__dirname + '/views/' + 'index.html' );
  console.log('Welcome....')
})

// get all images
// app.get('/upload',(req,res)=>{
//   console.log("get Route ")
//   fs.readFile(`${__dirname}/public/uploads`,function (err) {
//     if (err) throw err;
//     console.log('File read');
//   })
// })

// Upload Endpoint
app.post('/upload', (req, res) => {
  if (req.files === null) {
    return res.status(400).json({ msg: 'No file uploaded' });
  }
  console.log("Reques made")
  const file = req.files.file;

  file.mv(`${__dirname}/public/uploads/${file.name}`, err => {
    if (err) {
      console.error(err);
      return res.status(500).send(err);
    }

    res.json({ fileName: file.name, filePath: `http://localhost:5000/uploads/${file.name}` });
  });
});

app.listen(5000, () => console.log('Server Started...'));